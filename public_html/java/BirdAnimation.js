/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function BirdAnimation(centreX, centreY, width, height, birdSpeed, animationStartDelay)
{
    /* These three are ALWAYS needed */
    var animationInterval = null;
    var ANIMATION_SPEED = 120; // sprite sub-image changing speed. Smaller numbers give a faster animation */
    var animationIsDisplayed = false;

    /* These variables depend on the animation */
    var NUMBER_OF_SPRITES = 8;
    var currentSprite = 0;
    var row = 0;
    var column = 0;
    var SPRITE_WIDTH = parseInt((birdImage.width - 5) / NUMBER_OF_COLUMNS_IN_SPRITE_IMAGE); // the -5 is an adjustment so that this sprite works
    var SPRITE_HEIGHT = parseInt(birdImage.height / NUMBER_OF_ROWS_IN_SPRITE_IMAGE);

    /* Start the animation */
    setTimeout(start, animationStartDelay);


    /* Public functions */
    this.start = start;
    function start()
    {
        animationIsDisplayed = true;
        animationInterval = setInterval(update, ANIMATION_SPEED);
    }


    this.stop = stop;
    function stop()
    {
        animationIsDisplayed = true;
        clearInterval(animationInterval);
        animationInterval = null; // set to null when not running           
    }


    this.remove = remove;
    function remove()
    {
        stop();
        animationIsDisplayed = false;
    }


    this.update = update;
    function update()
    {
        centreX += birdSpeed;

        // if the sprite has gone off the right side of the screen, then reset it to be at the left side
        if (centreX >= CANVAS_WIDTH + (width / 2))
        {
            centreX = -width / 2;
        }

        currentSprite++;
        if (currentSprite === NUMBER_OF_SPRITES)
        {
            column = 0;
            row = 0;
            currentSprite = 0;
        }
        else
        {
            column++;
            if (column === NUMBER_OF_COLUMNS_IN_SPRITE_IMAGE)
            {
                column = 0;
                row++;
            }
        }
    }


    this.render = render;
    function render()
    {
        if (animationIsDisplayed)
        {
            ctx.drawImage(birdImage, column * SPRITE_WIDTH, row * SPRITE_WIDTH, SPRITE_WIDTH, SPRITE_HEIGHT, centreX - parseInt(width / 2), centreY - parseInt(height / 2), width, height);
        }
    }
}
