/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function BackgroundAnimation(speed)
{
    /* These three are ALWAYS needed */
    var animationInterval = null;
    var ANIMATION_SPEED = 20; // change to suit the animation speed in milliseconds. Smaller numbers give a faster animation */
    var animationIsDisplayed = false;

    /* These variables depend on the animation */
    var backgroundX = 0;


    /* Start the animation */
    setTimeout(start, 0);


    /* Public functions */
    this.start = start;
    function start()
    {
        animationIsDisplayed = true;
        animationInterval = setInterval(update, ANIMATION_SPEED);
    }


    this.stop = stop;
    function stop()
    {
        animationIsDisplayed = true;
        clearInterval(animationInterval);
        animationInterval = null; // set to null when not running           
    }


    this.kill = kill;
    function kill()
    {
        stop();
        animationIsDisplayed = false;
    }


    this.update = update;
    function update()
    {
        backgroundX -= speed;

        if (backgroundX <= -backgroundWidth)
        {
            backgroundX = 0;
        }
    }


    this.render = render;
    function render()
    {
        if (animationIsDisplayed)
        {
            //   ctx.drawImage(backgroundImage, 0, 0);
            ctx.drawImage(backgroundImage, backgroundX, 0, backgroundWidth, CANVAS_HEIGHT);
            // wrap image on canvas
            if (backgroundX <= -maxX)
            {
                ctx.drawImage(backgroundImage, backgroundX + backgroundWidth, 0, backgroundWidth, CANVAS_HEIGHT);
            }
        }
    }
}

